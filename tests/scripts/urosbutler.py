#!/bin/env python

'''
UROS butler
Title: urosbutler
Author: Alessandro Thea and Tom Williams (adapted to uROS by Alvaro Navarro)

Example usage ...

urosbutler.py show
urosbutler.py reset MY_BOARD --clksrc=internal
urosbutler.py ...

'''

import logging
import mp7

from mp7.cli_core import CLIEngine

from uros.cmds import plugins as cmds
from uros.tools.helpers import logo

if __name__ == '__main__':

    cli = CLIEngine(logo=logo)
    cli.setDefaultConnectionFiles(['file://${UROS_TESTS}/etc/uros/connections-'+x+'.xml' for x in ['uros']])

    for cmd in cmds:
        cli.addCommand(cmd)

    try:
        global result
        result = cli.run()
    except mp7.exception as e:
        logging.critical("%s",e)
    except RuntimeError as re:
        logging.critical("%s",re)

