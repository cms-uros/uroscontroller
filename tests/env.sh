function pathadd() {
  # TODO add check for empty path
  # and what happens if $1 == $2
  # Copy into temp variables
  PATH_NAME=$1
  PATH_VAL=${!1}
  if [[ ":$PATH_VAL:" != *":$2:"* ]]; then
    PATH_VAL="$2${PATH_VAL:+":$PATH_VAL"}"
    echo "- $1 += $2"

    # use eval to reset the target
    eval "$PATH_NAME=$PATH_VAL"
  fi

}


CACTUS_ROOT=/opt/cactus
UROS_TESTS=$( readlink -f $(dirname $BASH_SOURCE)/ )
UROS_ROOT=$( readlink -f ${UROS_TESTS}/.. )

pathadd LD_LIBRARY_PATH "${CACTUS_ROOT}/lib"

# add to path
pathadd PATH "${UROS_ROOT}/tests/scripts"
#pathadd PATH "${MP7_ROOT}/tests/scripts/tmt"

# add python path
pathadd PYTHONPATH "${UROS_ROOT}/pycouros/pkg"
pathadd PYTHONPATH "${UROS_ROOT}/tests/python"

# add libary path
pathadd LD_LIBRARY_PATH "${UROS_ROOT}/uros/lib"

export CACTUS_ROOT UROS_TESTS UROS_ROOT LD_LIBRARY_PATH PYTHONPATH
