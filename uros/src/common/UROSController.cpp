#include <time.h>
#include <stdio.h>

/// UROS headers
#include "uros/UROSController.hpp"

/// MP7 headers
#include "mp7/CtrlNode.hpp"
#include "mp7/Logger.hpp"
#include "mp7/TTCNode.hpp"

void uros::UROSController::mapTest1(const std::map<uint32_t, uint32_t>& params) const { printf("Element at key %i is %i\n",0, params.at(0)); }
void uros::UROSController::mapTest2(const std::map<std::string, uint32_t>& params) const { printf("Element at key %s is %i\n","key", params.at("key")); }

///------------------------------------------------------------------
///--- Constructor & misc general functions
///------------------------------------------------------------------
uros::UROSController::UROSController(const uhal::HwInterface& aHw ) : 
      mHw( aHw )
{ 
}

uhal::HwInterface& uros::UROSController::hw() {
  return mHw;
}

inline void uros::UROSController::dispatch(void) { 
  hw().dispatch(); 
}

inline const uhal::Node& uros::UROSController::getNode( const std::string& aId) { 
  return ( hw().getNode(aId) );
}

void uros::UROSController::write(const std::string& regname, uint32_t value, bool dodispatch) {
  try { getNode( regname ).write( value );  }
  catch ( const uhal::exception::BitsSetWhichAreForbiddenByBitMask& e ) {
    std::string message = "[" + regname + "] " + e.what();
    uhal::exception::BitsSetWhichAreForbiddenByBitMask e2;
    e2.append(message.c_str());
    throw e2;
  }
  if (dodispatch) dispatch();
}

uint32_t uros::UROSController::read(const std::string& regname) {
  uhal::ValWord<uint32_t> value = getNode( regname ).read();
  dispatch();
  return (uint32_t) value;
}

///------------------------------------------------------------------
///--- MP7-RELATED
///------------------------------------------------------------------

void uros::UROSController::reset(const std::string& aClkSrc) {
  const mp7::CtrlNode& mCtrl(mHw.getNode<mp7::CtrlNode>("ctrl"));
  const mp7::TTCNode& mTTC(mHw.getNode<mp7::TTCNode>("ttc"));

  mCtrl.softReset();

  { /// DO NOT REMOVE WITHOUT KNOWLEDGE!!!
    /// Hold clk4 in reset
    mp7::CtrlNode::Clock40Guard guard(mCtrl, 5.0);
    MP7_LOG( mp7::logger::kInfo ) << "[UROS_" << mHw.id() << "] selecting clock source ";
    mCtrl.selectClk40Source(true);
  } /// guard goes out of scope here, clk40 is released

  MP7_LOG( mp7::logger::kInfo ) << "[UROS_" << mHw.id() << "] waitClk40Lock";
  mCtrl.waitClk40Lock();

  /// TTC Configuration
  /// Takes place AFTER clock40 is released
  std::string lPath = MP7_ETC_DEFAULT ;
  if(char* lEnvVar = std::getenv("MP7_ETC")){
    lPath = lEnvVar;
    lPath += "/mp7";
  }
  mp7::TTCConfigurator ttcCfgtor("external.xml", "r1", lPath);

  /// Ensure that the master clock choice is compatible with the ttc configuration
  MP7_LOG( mp7::logger::kInfo ) << "[UROS_" << mHw.id() << "] configure(mTTC)";
  ttcCfgtor.configure(mTTC);
}
