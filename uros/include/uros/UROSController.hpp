/*
* @file    UROSController.hpp
* @author  Alvaro Navarro
* @brief   UROS driver implementation: reuses mp7 where possible
* @date    27/3/17
*/

#ifndef _uros_board_UROSController_hpp_
#define _uros_board_UROSController_hpp_

// uHal Headers
#include "uhal/HwInterface.hpp"
// MP7 headers
#include "mp7/MP7MiniController.hpp"

#include <map>

namespace uros {
  
  class UROSController : public boost::noncopyable {

    public:

      /// Constructor
      UROSController(const uhal::HwInterface& aHw );

      /// Destructor
      ~UROSController() {};

      /// uHal commands
      void dispatch(void);
      const uhal::Node& getNode(const std::string& aId);
      void write(const std::string& regname, uint32_t value, bool dodispatch = true);
      uint32_t read(const std::string& regname);
      uhal::HwInterface& hw();

      /// Misc
      void start(){};
      void mapTest1(const std::map<uint32_t,    uint32_t>&params) const;
      void mapTest2(const std::map<std::string, uint32_t>&params) const;

      /// MP7-related
      void reset(const std::string& aClkSrc);

    private:
      inline std::string str(int i) { return std::to_string((long long int)i); } ///makes it easier. remove when don't need gcc 4.4.7 anymore
      uhal::HwInterface mHw; //! IPBus interface to the MP7 board
  };
}

#endif
