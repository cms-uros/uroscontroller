from mp7.cli_plugins import DevicePlugin, _log

from uros import UROSController
#---
class UROSPlugin(DevicePlugin):

    def __init__(self, *args, **kwargs):
        super(UROSPlugin,self).__init__(*args, **kwargs)

    @classmethod
    def _addArgs(cls, subp):
        _log.debug(" > Entering:  UROSPlugin._addArgs")
        super(UROSPlugin, cls)._addArgs(subp)
        _log.debug(" > Meat of :  UROSPlugin._addArgs")
        _log.debug(" > Exiting :  UROSPlugin._addArgs")

    @classmethod
    def prepare(cls, kwargs):
        _log.debug(" > Entering:  UROSPlugin.prepare")
        super(UROSPlugin, cls).prepare(kwargs)
        _log.debug(" > Meat of :  UROSPlugin.prepare")

        # Create the subject
        board = UROSController( kwargs['board'] )

        # Replace board id with the controller itself
        kwargs['board'] = board

        _log.debug(" > Exiting :  UROSPlugin.prepare")
        #board.identify()
