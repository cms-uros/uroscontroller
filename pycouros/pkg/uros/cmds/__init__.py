
#from mp7.cmds import CommandAdaptor, DevicesLister, PythonCommand,  MINICOMMANDS
#from mp7.cli_plugins import Command, DeviceCommand
from uros.cli_plugins import UROSPlugin

#import mp7.tools.helpers as hlp

import infra
import mp7.cmds

plugins = mp7.cmds.Factory.makeMP7Mini(UROSPlugin)

plugins += [UROSPlugin('reset', 'Reset the board and set the clocking', infra.Reset() )]
