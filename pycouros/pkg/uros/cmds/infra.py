import logging

from mp7.cli_core import defaultFmtStr, FunctorInterface
import mp7

class Reset(FunctorInterface):

    @staticmethod
    def addArgs(subp):
        clksrcs = ['internal', 'external']
        subp.add_argument('--clksrc', choices=clksrcs, default='external', help='Clock source selection'+defaultFmtStr)

    @staticmethod
    def run(board, clksrc='external', check=True):
        logging.info('Board master reset')
        
        board.reset(clksrc)

