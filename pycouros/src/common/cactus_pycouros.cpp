
// Boost Headers
#include <boost/python/module.hpp>
#include <boost/python/def.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <boost/python/wrapper.hpp>
#include <boost/python/enum.hpp>
#include <boost/python/operators.hpp>
#include <boost/python/copy_const_reference.hpp>
#include <boost/python/overloads.hpp>
//#include <boost/python.hpp>

// C++ Headers
#include "map"

// uHal Headers
#include "uhal/uhal.hpp"
#include "uhal/Node.hpp"

// UROS Headers
#include "uros/UROSController.hpp"

using namespace boost::python;


// *** N.B: The argument of this BOOST_PYTHON_MODULE macro MUST be the same as the name of the library created, i.e. if creating library file my_py_binds_module.so , imported in python as:
//                import my_py_binds_module
//          then would have to put
//                BOOST_PYTHON_MODULE(my_py_binds_module)
//          Otherwise, will get the error message "ImportError: dynamic module does not define init function (initmy_py_binds_module)
BOOST_PYTHON_MODULE(_pycouros) {

  class_<uros::UROSController, boost::noncopyable >("UROSController", init<const uhal::HwInterface& >() )
    /// uHal commands
    .def("dispatch",              &uros::UROSController::dispatch )
    .def("hw",                    &uros::UROSController::hw, return_internal_reference<>() )
    /// Misc
    .def("start",                 &uros::UROSController::start )
    .def("mapTest1",              static_cast<void (uros::UROSController::*)(const std::map<uint32_t,    uint32_t>& ) const>(&uros::UROSController::mapTest1))
    .def("mapTest2",              static_cast<void (uros::UROSController::*)(const std::map<std::string, uint32_t>& ) const>(&uros::UROSController::mapTest2))
    /// MP7-related
    .def("reset",                 &uros::UROSController::reset )
    ;
}

