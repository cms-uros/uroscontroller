#!/bin/bash

# XDAQ
export XDAQ_ROOT=/opt/xdaq
export XDAQ_DOCUMENT_ROOT=$XDAQ_ROOT/htdocs
export XDAQ_SETUP_ROOT=$XDAQ_ROOT/share

# CACTUS
export CACTUS_ROOT=/opt/cactus

# LIBS & BINS
export LD_LIBRARY_PATH=$CACTUS_ROOT/lib:$XDAQ_ROOT/lib:/usr/lib:$LD_LIBRARY_PATH
export PATH=/opt/cactus/bin:$PATH

